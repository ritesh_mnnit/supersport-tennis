﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using System.Xml;
using Xceed.Zip;
using Xceed.FileSystem;
using System.Runtime.CompilerServices;
using Common.Logging;

[assembly: InternalsVisibleTo("ssTennis.Tests")]
namespace ssTennis
{
    public partial class ssTennis : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        runInfo curRun;

        private ILog _logger = null;

        public ssTennis()
        {
            InitializeComponent();

            _logger = LogManager.GetLogger(typeof(ssTennis));
        }

        internal void TestStartupAndStop(string[] args)
        {
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A";
            run();
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += timerElapsed;
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
            //Console.ReadLine();
            Thread.Sleep(600000);
        }

        protected override void OnStart(string[] args)
        {
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A";
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        internal void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;

            _logger.Info("Processing Started.");

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in run()", ex);

                dbConnected = false;
            }

            if (dbConnected)
            {
                dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);

                archiveFiles();
                getFiles();
                processFiles();

                dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
            }

            curRun.End = DateTime.Now;
            endRun();
            writeInfo(curRun.End.ToString("s"));
            _logger.Info("Processing Ended.");

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(60000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_Runs (startTime, endTime, downloads, downloadErrors, errors, timeStamp) Values (@startTime, @endTime, @downloads, @downloadErrors, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@downloads", SqlDbType.Int).Value = curRun.Downloads;
                SqlQuery.Parameters.Add("@downloadErrors", SqlDbType.Int).Value = curRun.DownloadErrors;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in endRun()", ex);

                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in dbConnChanged()", ex);

                        dbConnected = false;
                    }
                }
            }
        }

        private void writeInfo(string Date)
        {
            try
            {
                string tmpDate = Date.Replace(':', '_').Replace('/', '_');
                FileStream Fs = new FileStream("c:/SuperSport/Tennis/LastRun/lastRun_"+ tmpDate +".txt", FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.End.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Downloads: " + curRun.Downloads.ToString());
                Sw.WriteLine("Download Errors: " + curRun.DownloadErrors.ToString());
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in writeInfo()", ex);
            }
        }

        private void getFiles()
        {
            try
            {
                _logger.Info("Retrieving Files for processing.");
                FileInfo fi;

                if (Directory.Exists("C:/inetpub/ftproot/pauser/tennis"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/pauser/tennis"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            fi.CopyTo("c:/SuperSport/Tennis/Xml/Temp/" + fi.Name);
                            fi.Delete();
                        }
                    }
                }

                if (Directory.Exists("C:/inetpub/ftproot/teamtalk/tennis"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/teamtalk/tennis"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            fi.CopyTo("c:/SuperSport/Tennis/Xml/Temp/" + fi.Name);
                            fi.Delete();
                        }
                    }
                }
                _logger.Info("Retrieving Files for processing.");
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in getFiles()", ex);

                curRun.Errors.Add(ex.Message.ToString());
            }
        }

        private void processFiles(bool test = false)
        {
            if (test)
            {
                dbConn = new SqlConnection("Server=197.80.203.40;UID=supersms;PWD=5m5RrR4;DATABASE=SuperSportZone");
                dbConn.Open();
            }

            _logger.Info("Processing of Files started.");

            foreach (string tmpFile in Directory.GetFiles("c:/SuperSport/Tennis/Xml/Temp"))
            {
                FileInfo Fi = new FileInfo(tmpFile);
                bool Done = false;

                if (Fi.Name.ToLower().IndexOf(".xml") >= 0)
                {
                    if (Fi.Name.ToLower().IndexOf("patenniscompetitions") >= 0)
                    {
                        Done = processCompetitions(Fi.Name);
                    }
                    else if (Fi.Name.ToLower().IndexOf("tns_fix") >= 0)
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessFixtures"]))
                            Done = processFixtures(Fi.Name);
                        else
                            Done = true;
                    }
                    else if (Fi.Name.ToLower().IndexOf("tns_mch") >= 0)
                    {
                        Done = processMatches(Fi.Name);
                    }
                    else if (Fi.Name.ToLower().IndexOf("tns_oop") >= 0)
                    {
                        Done = processOrderOfPlay(Fi.Name);
                    }
                    else if (Fi.Name.ToLower().IndexOf("tns_drw") >= 0)
                    {
                        Done = processDraw(Fi.Name);
                    }
                    else if (Fi.Name.ToLower().IndexOf("tns_rnk") >= 0)
                    {
                        Done = processRankings(Fi.Name);
                    }
                }
                else
                {
                    Done = true;
                }

                if (Done)
                {
                    if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                    {
                        Fi.CopyTo("c:/SuperSport/Tennis/Xml/Processed/" + Fi.Name, true);
                    }
                }
                else
                {
                    if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                    {
                        Fi.CopyTo("c:/SuperSport/Tennis/Xml/Unprocessed/" + Fi.Name, true);
                    }
                }
                if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                {
                    Fi.Delete();
                }
            }

            _logger.Info("Processing of Files ended.");

            if (test)
            {
                dbConn.Close();
            }
        }

        private bool processCompetitions(string fileName)
        {
            bool Processed = true;

            try
            {
                _logger.Info("Processing of competitions for file "+ fileName +" started.");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                SqlCommand SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_Competitions Where (Year = @Year)", dbConn);
                SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("PATennisCompetitions"))
                {
                    DateTime TimeStamp = DateTime.Now;
                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "timestamp":
                                string tmpDate = MainAttribute.Value.ToString();
                                int timeStampYear = Convert.ToInt32(tmpDate.Substring(0, 4));
                                int timeStampMonth = Convert.ToInt32(tmpDate.Substring(4, 2));
                                int timeStampDay = Convert.ToInt32(tmpDate.Substring(6, 2));
                                int timeStampHour = Convert.ToInt32(tmpDate.Substring(9, 2));
                                int timeStampMinute = Convert.ToInt32(tmpDate.Substring(11, 2));
                                int timeStampSecond = Convert.ToInt32(tmpDate.Substring(13, 2));
                                TimeStamp = new DateTime(timeStampYear, timeStampMonth, timeStampDay, timeStampHour, timeStampMinute, timeStampSecond);
                                break;
                            default:
                                break;
                        }
                    }
                    foreach (XmlNode CompNode in MainNode.SelectNodes("Competition"))
                    {
                        string compId = "";
                        string Title = "";
                        string Sponsor = "";
                        string Type = "";
                        foreach (XmlAttribute CompAttribute in CompNode.Attributes)
                        {
                            switch (CompAttribute.Name)
                            {
                                case "compid":
                                    compId = CompAttribute.Value;
                                    break;
                                case "title":
                                    Title = CompAttribute.Value;
                                    break;
                                case "sponsor":
                                    Sponsor = CompAttribute.Value;
                                    break;
                                case "type":
                                    Type = CompAttribute.Value;
                                    break;
                                default:
                                    break;
                            }
                        }
                        SqlQuery = new SqlCommand("Insert Into Tennis.dbo.pa_Competitions (competitionId, Year, Title, Sponsor, timeStamp, Type) Values (@competitionId, @Year, @title, @Sponsor, @timeStamp, @Type)", dbConn);
                        SqlQuery.Parameters.Add("@competitionId", SqlDbType.VarChar).Value = compId;
                        SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = Title;
                        SqlQuery.Parameters.Add("@Sponsor", SqlDbType.VarChar).Value = Sponsor;
                        SqlQuery.Parameters.Add("@Type", SqlDbType.VarChar).Value = Type;
                        SqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = TimeStamp;
                        SqlQuery.ExecuteNonQuery();
                    }
                }

                _logger.Info("Processing of competitions for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processCompetitions()", ex);

                Processed = false;
                curRun.Errors.Add(fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool processFixtures(string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;
            int tmpId = 0;

            try
            {
                _logger.Info("Processing of fixtures for file " + fileName + " started.");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("TennisFixtures"))
                {
                    DateTime fixturesStartDate = DateTime.Now;
                    DateTime fixturesEndDate = DateTime.Now;

                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "startdate":
                                fixturesStartDate = new DateTime(int.Parse(ConfigurationManager.AppSettings["CurrentTournamentYear"]), 1, 1);
                                //fixturesStartDate = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                //    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                //    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            case "enddate":
                                fixturesEndDate = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            default:
                                break;
                        }
                    }

                    //SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_fixtures Where (Year = @year)", dbConn);
                    //SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = fixturesStartDate.Year;
                    //SqlQuery.ExecuteNonQuery();

                    foreach (XmlNode CompNode in MainNode.SelectNodes("Fixture"))
                    {
                        int Id = -1;
                        int competitionId = -1;
                        int stageNumber = -1;
                        int roundNumber = -1;
                        int Leg = -1;
                        string Type = "";
                        string Title = "";
                        DateTime startDate = DateTime.Now;
                        DateTime endDate = DateTime.Now;
                        string Location = "";
                        string Country = "";
                        string Surface = "";
                        string prizeMoney = "";
                        string prizeCurrency = "";

                        foreach (XmlAttribute CompAttribute in CompNode.Attributes)
                        {
                            switch (CompAttribute.Name)
                            {
                                case "id":
                                    Id = Convert.ToInt32(CompAttribute.Value);
                                    tmpId = Id;
                                    break;
                                case "competitionId":
                                    competitionId = Convert.ToInt32(CompAttribute.Value);
                                    break;
                                case "stageNumber":
                                    stageNumber = Convert.ToInt32(CompAttribute.Value);
                                    break;
                                case "roundNumber":
                                    roundNumber = Convert.ToInt32(CompAttribute.Value);
                                    break;
                                case "leg":
                                    Leg = Convert.ToInt32(CompAttribute.Value);
                                    break;
                                case "type":
                                    Type = CompAttribute.Value.ToString();
                                    break;
                                case "title":
                                    Title = CompAttribute.Value.ToString();
                                    break;
                                case "startdate":
                                    startDate = new DateTime(Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[2]),
                                        Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[1]),
                                        Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[0]));
                                    break;
                                case "enddate":
                                    endDate = new DateTime(Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[2]),
                                        Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[1]),
                                        Convert.ToInt32(CompAttribute.Value.Replace("/", "-").Split('-')[0]));
                                    break;
                                case "location":
                                    Location = CompAttribute.Value.ToString();
                                    break;
                                case "country":
                                    Country = CompAttribute.Value.ToString();
                                    break;
                                case "surfaceType":
                                    Surface = CompAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode PrizeNode in CompNode.SelectNodes("PrizeMoney"))
                        {
                            foreach (XmlAttribute PrizeAttribute in PrizeNode.Attributes)
                            {
                                switch (PrizeAttribute.Name)
                                {
                                    case "value":
                                        prizeMoney = PrizeAttribute.Value.ToString();
                                        break;
                                    case "currency":
                                        prizeCurrency = PrizeAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        SqlQuery = new SqlCommand("SELECT COUNT(Id) FROM tennis.dbo.pa_Fixtures WHERE (Year = @year) AND (Id = @Id) AND (CompetitionId = @CompetitionId)", dbConn);
                        SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                        SqlQuery.Parameters.Add("@CompetitionId", SqlDbType.Int).Value = competitionId;
                        SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = fixturesStartDate.Year;
                        int records = Convert.ToInt32(SqlQuery.ExecuteScalar());

                        if (records > 0)
                        {
                            SqlQuery = new SqlCommand(@"UPDATE tennis.dbo.pa_Fixtures Set Id = @Id, CompetitionId = @CompetitionId, Year = @year, fixturesStartDate = @fixturesStartDate,
                                                        fixturesEndDate = @fixturesEndDate, type = @type, startDate = @startDate, endDate = @endDate, location = @location, surface = @surface,
                                                        prizeMoney = @prizeMoney, currency = @currency, stageNumber = @stageNumber, roundNumber = @roundNumber, leg = @leg, title = @title,
                                                        country = @country  WHERE (Year = @year) AND (Id = @Id) AND (CompetitionId = @CompetitionId)", dbConn);
                        }
                        else
                        {
                            SqlQuery = new SqlCommand(@"Insert Into tennis.dbo.pa_Fixtures (Id, CompetitionId, Year, fixturesStartDate, fixturesEndDate, type, startDate, endDate, location, surface,
                                                        prizeMoney, currency, stageNumber, roundNumber, leg, title, country) Values (@Id, @CompetitionId, @Year, @fixturesStartDate, @fixturesEndDate,
                                                        @type, @startDate, @endDate, @location, @surface, @prizeMoney, @currency, @stageNumber, @roundNumber, @leg, @title, @country)", dbConn);
                        }
                        SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                        SqlQuery.Parameters.Add("@CompetitionId", SqlDbType.Int).Value = competitionId;
                        SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = fixturesStartDate.Year;
                        SqlQuery.Parameters.Add("@fixturesStartDate", SqlDbType.VarChar).Value = fixturesStartDate;
                        SqlQuery.Parameters.Add("@fixturesEndDate", SqlDbType.VarChar).Value = fixturesEndDate;
                        SqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = Type;
                        SqlQuery.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        SqlQuery.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        SqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = Location;
                        SqlQuery.Parameters.Add("@surface", SqlDbType.VarChar).Value = Surface;
                        SqlQuery.Parameters.Add("@prizeMoney", SqlDbType.VarChar).Value = prizeMoney;
                        SqlQuery.Parameters.Add("@currency", SqlDbType.VarChar).Value = prizeCurrency;
                        SqlQuery.Parameters.Add("@stageNumber", SqlDbType.Int).Value = stageNumber;
                        SqlQuery.Parameters.Add("@roundNumber", SqlDbType.Int).Value = roundNumber;
                        SqlQuery.Parameters.Add("@leg", SqlDbType.Int).Value = Leg;
                        SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = Title;
                        SqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = Country;
                        SqlQuery.ExecuteNonQuery();
                    }
                }

                _logger.Info("Processing of fixtures for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processFixtures()", ex);

                Processed = false;
                curRun.Errors.Add(tmpId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool processMatches(string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                _logger.Info("Processing of matches for file " + fileName + " started.");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("TennisMatch"))
                {
                    int matchId = -1;
                    int roundId = -1;
                    int tournamentId = -1;
                    string Status = "";
                    string compType = "";
                    string roundType = "";
                    int drawOrder = -1;
                    int roundOrder = -1;
                    DateTime Date = DateTime.Now;
                    string Court = "";
                    int Sequence = -1;
                    int numberOfSets = -1;
                    string winningSide = "";
                    string statusText = "";
                    string Comment = "";
                    DateTime timeStamp = DateTime.Now;

                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "matchid":
                                matchId = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "roundid":
                                roundId = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "tournamentid":
                                tournamentId = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "status":
                                Status = MainAttribute.Value.ToString();
                                break;
                            case "comptype":
                                compType = MainAttribute.Value.ToString();
                                break;
                            case "roundtype":
                                roundType = MainAttribute.Value.ToString();
                                break;
                            case "draworder":
                                drawOrder = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "roundorder":
                                bool result = Int32.TryParse(MainAttribute.Value, out roundOrder);
                                break;
                            case "date":
                                Date = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            case "court":
                                Court = MainAttribute.Value.ToString();
                                break;
                            case "sequence":
                                Sequence = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "numberofsets":
                                numberOfSets = Convert.ToInt32(MainAttribute.Value);
                                break;
                            case "winningside":
                                winningSide = MainAttribute.Value.ToString();
                                break;
                            case "timestamp":
                                timeStamp = Convert.ToDateTime(MainAttribute.Value);
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode statusNode in MainNode.SelectNodes("StatusText"))
                    {
                        statusText = statusNode.InnerText;
                    }

                    foreach (XmlNode statusNode in MainNode.SelectNodes("Comment"))
                    {
                        Comment = statusNode.InnerText;
                    }

                    int sideOnePlayer1Id = -1;
                    int sideOnePlayer2Id = -1;
                    int sideTwoPlayer1Id = -1;
                    int sideTwoPlayer2Id = -1;

                    string sideOnePlayer1Lastname = "";
                    string sideOnePlayer2Lastname = "";
                    string sideTwoPlayer1Lastname = "";
                    string sideTwoPlayer2Lastname = "";

                    string sideOnePlayer1Firstname = "";
                    string sideOnePlayer2Firstname = "";
                    string sideTwoPlayer1Firstname = "";
                    string sideTwoPlayer2Firstname = "";

                    string sideOnePlayer1Country = "";
                    string sideOnePlayer2Country = "";
                    string sideTwoPlayer1Country = "";
                    string sideTwoPlayer2Country = "";

                    int sideOnePlayer1Seed = 1000;
                    int sideOnePlayer2Seed = 1000;
                    int sideTwoPlayer1Seed = 1000;
                    int sideTwoPlayer2Seed = 1000;

                    string scoreString = "";

                    foreach (XmlNode playerNode in MainNode.SelectNodes("SideOne"))
                    {
                        foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerOne"))
                        {
                            foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                            {
                                switch (playerAttribute.Name)
                                {
                                    case "playerid":
                                        sideOnePlayer1Id = Convert.ToInt32(playerAttribute.Value);
                                        break;
                                    case "lastname":
                                        sideOnePlayer1Lastname = playerAttribute.Value.ToString();
                                        break;
                                    case "firstname":
                                        sideOnePlayer1Firstname = playerAttribute.Value.ToString();
                                        break;
                                    case "country":
                                        sideOnePlayer1Country = playerAttribute.Value.ToString();
                                        break;
                                    case "seed":
                                        try
                                        {
                                            sideOnePlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                        }
                                        catch (Exception seedException)
                                        {
                                            _logger.Error("Error occurred in processMatches()", seedException);

                                            sideOnePlayer1Seed = 1000;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerTwo"))
                        {
                            foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                            {
                                switch (playerAttribute.Name)
                                {
                                    case "playerid":
                                        sideOnePlayer2Id = Convert.ToInt32(playerAttribute.Value);
                                        break;
                                    case "lastname":
                                        sideOnePlayer2Lastname = playerAttribute.Value.ToString();
                                        break;
                                    case "firstname":
                                        sideOnePlayer2Firstname = playerAttribute.Value.ToString();
                                        break;
                                    case "country":
                                        sideOnePlayer2Country = playerAttribute.Value.ToString();
                                        break;
                                    case "seed":
                                        try
                                        {
                                            sideOnePlayer2Seed = Convert.ToInt32(playerAttribute.Value);
                                        }
                                        catch (Exception seedException)
                                        {
                                            _logger.Error("Error occurred in processMatches()", seedException);

                                            sideOnePlayer2Seed = 1000;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        if (scoreString != "")
                        {
                            scoreString += Environment.NewLine;
                        }
                        scoreString += "Delete From tennis.dbo.pa_Scores Where (matchId = " + matchId + " And Side = 1);";

                        foreach (XmlNode setSubNode in playerNode.SelectNodes("Set"))
                        {
                            int setNumber = -1;
                            int Games = -1;
                            int tieBreak = -1;
                            foreach (XmlAttribute setAttribute in setSubNode.Attributes)
                            {
                                switch (setAttribute.Name)
                                {
                                    case "setnumber":
                                        setNumber = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    case "games":
                                        Games = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    case "tiebreak":
                                        tieBreak = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (scoreString != "")
                            {
                                scoreString += Environment.NewLine;
                            }
                            scoreString += "Insert Into tennis.dbo.pa_Scores (matchId, side, setNumber, Games, tieBreak) Values (" + matchId + ", 1, " + setNumber + ", " + Games + ", " + tieBreak + ");";
                        }
                    }

                    foreach (XmlNode playerNode in MainNode.SelectNodes("SideTwo"))
                    {
                        foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerOne"))
                        {
                            foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                            {
                                switch (playerAttribute.Name)
                                {
                                    case "playerid":
                                        sideTwoPlayer1Id = Convert.ToInt32(playerAttribute.Value);
                                        break;
                                    case "lastname":
                                        sideTwoPlayer1Lastname = playerAttribute.Value.ToString();
                                        break;
                                    case "firstname":
                                        sideTwoPlayer1Firstname = playerAttribute.Value.ToString();
                                        break;
                                    case "country":
                                        sideTwoPlayer1Country = playerAttribute.Value.ToString();
                                        break;
                                    case "seed":
                                        try
                                        {
                                            sideTwoPlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                        }
                                        catch (Exception seedException)
                                        {
                                            _logger.Error("Error occurred in processMatches()", seedException);

                                            sideTwoPlayer1Seed = 1000;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerTwo"))
                        {
                            foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                            {
                                switch (playerAttribute.Name)
                                {
                                    case "playerid":
                                        sideTwoPlayer2Id = Convert.ToInt32(playerAttribute.Value);
                                        break;
                                    case "lastname":
                                        sideTwoPlayer2Lastname = playerAttribute.Value.ToString();
                                        break;
                                    case "firstname":
                                        sideTwoPlayer2Firstname = playerAttribute.Value.ToString();
                                        break;
                                    case "country":
                                        sideTwoPlayer2Country = playerAttribute.Value.ToString();
                                        break;
                                    case "seed":
                                        try
                                        {
                                            sideTwoPlayer2Seed = Convert.ToInt32(playerAttribute.Value);
                                        }
                                        catch (Exception seedException)
                                        {
                                            _logger.Error("Error occurred in processMatches()", seedException);

                                            sideTwoPlayer2Seed = 1000;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        if (scoreString != "")
                        {
                            scoreString += Environment.NewLine;
                        }
                        scoreString += "Delete From tennis.dbo.pa_Scores Where (matchId = " + matchId + " And Side = 2);";

                        foreach (XmlNode setSubNode in playerNode.SelectNodes("Set"))
                        {
                            int setNumber = -1;
                            int Games = -1;
                            int tieBreak = -1;
                            foreach (XmlAttribute setAttribute in setSubNode.Attributes)
                            {
                                switch (setAttribute.Name)
                                {
                                    case "setnumber":
                                        setNumber = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    case "games":
                                        Games = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    case "tiebreak":
                                        tieBreak = Convert.ToInt32(setAttribute.Value);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (scoreString != "")
                            {
                                scoreString += Environment.NewLine;
                            }
                            scoreString += "Insert Into tennis.dbo.pa_Scores (matchId, side, setNumber, Games, tieBreak) Values (" + matchId + ", 2, " + setNumber + ", " + Games + ", " + tieBreak + ");";
                        }
                    }

                    if (scoreString != "")
                    {
                        SqlQuery = new SqlCommand(scoreString, dbConn);
                        SqlQuery.ExecuteNonQuery();
                    }

                    SqlQuery = new SqlCommand("Select Count(Id) From tennis.dbo.pa_Matches Where (Id = @matchId)", dbConn);
                    SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId;
                    int Present = Convert.ToInt32(SqlQuery.ExecuteScalar());

                    if (Present > 0)
                    {
                        SqlQuery = new SqlCommand(@"Update tennis.dbo.pa_Matches Set roundId = @roundId, tournamentId = @tournamentId, Status = @Status,
                                                    Type = @compType, roundType = @roundType, drawOrder = @drawOrder, roundOrder = @roundOrder, Date = @Date,
                                                    Court = @Court, Sequence = @Sequence, Sets = @numberOfSets, winningSide = @winningSide, statusText = @statusText,
                                                    Comment = @Comment, timeStamp = @timeStamp, sideOnePlayer1Id = @sideOnePlayer1Id, sideOnePlayer1Lastname = @sideOnePlayer1Lastname,
                                                    sideOnePlayer1Firstname = @sideOnePlayer1Firstname, sideOnePlayer1Country = @sideOnePlayer1Country, sideOnePlayer1Seed = @sideOnePlayer1Seed,
                                                    sideOnePlayer2Id = @sideOnePlayer2Id, sideOnePlayer2Lastname = @sideOnePlayer2Lastname, sideOnePlayer2Firstname = @sideOnePlayer2Firstname,
                                                    sideOnePlayer2Country = @sideOnePlayer2Country, sideOnePlayer2Seed = @sideOnePlayer2Seed, sideTwoPlayer1Id = @sideTwoPlayer1Id,
                                                    sideTwoPlayer1Lastname = @sideTwoPlayer1Lastname, sideTwoPlayer1Firstname = @sideTwoPlayer1Firstname, sideTwoPlayer1Country = @sideTwoPlayer1Country,
                                                    sideTwoPlayer1Seed = @sideTwoPlayer1Seed, sideTwoPlayer2Id = @sideTwoPlayer2Id, sideTwoPlayer2Lastname = @sideTwoPlayer2Lastname,
                                                    sideTwoPlayer2Firstname = @sideTwoPlayer2Firstname, sideTwoPlayer2Country = @sideTwoPlayer2Country, sideTwoPlayer2Seed = @sideTwoPlayer2Seed
                                                    Where (Id = @matchId)", dbConn);
                    }
                    else
                    {
                        SqlQuery = new SqlCommand(@"Insert Into tennis.dbo.pa_Matches (Id, roundId, tournamentId, Status, Type, roundType, drawOrder, roundOrder, Date, Court, Sequence, Sets, winningSide,
                                                    statusText, Comment, timeStamp, sideOnePlayer1Id, sideOnePlayer1Lastname, sideOnePlayer1Firstname, sideOnePlayer1Country, sideOnePlayer1Seed,
                                                    sideOnePlayer2Id, sideOnePlayer2Lastname, sideOnePlayer2Firstname, sideOnePlayer2Country, sideOnePlayer2Seed, sideTwoPlayer1Id,
                                                    sideTwoPlayer1Lastname, sideTwoPlayer1Firstname, sideTwoPlayer1Country, sideTwoPlayer1Seed, sideTwoPlayer2Id, sideTwoPlayer2Lastname,
                                                    sideTwoPlayer2Firstname, sideTwoPlayer2Country, sideTwoPlayer2Seed) Values (@matchId, @roundId, @tournamentId, @Status, @compType, @roundType,
                                                    @drawOrder, @roundOrder, @Date, @Court, @Sequence, @numberOfSets, @winningSide, @statusText, @Comment, @timeStamp, @sideOnePlayer1Id,
                                                    @sideOnePlayer1Lastname, @sideOnePlayer1Firstname, @sideOnePlayer1Country, @sideOnePlayer1Seed, @sideOnePlayer2Id, @sideOnePlayer2Lastname,
                                                    @sideOnePlayer2Firstname, @sideOnePlayer2Country, @sideOnePlayer2Seed, @sideTwoPlayer1Id, @sideTwoPlayer1Lastname, @sideTwoPlayer1Firstname,
                                                    @sideTwoPlayer1Country, @sideTwoPlayer1Seed, @sideTwoPlayer2Id, @sideTwoPlayer2Lastname, @sideTwoPlayer2Firstname, @sideTwoPlayer2Country,
                                                    @sideTwoPlayer2Seed)", dbConn);
                    }
                    SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@tournamentId", SqlDbType.Int).Value = tournamentId;
                    SqlQuery.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                    SqlQuery.Parameters.Add("@compType", SqlDbType.VarChar).Value = compType;
                    SqlQuery.Parameters.Add("@roundType", SqlDbType.VarChar).Value = roundType;
                    SqlQuery.Parameters.Add("@drawOrder", SqlDbType.Int).Value = drawOrder;
                    SqlQuery.Parameters.Add("@roundOrder", SqlDbType.Int).Value = roundOrder;
                    SqlQuery.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                    SqlQuery.Parameters.Add("@Court", SqlDbType.VarChar).Value = Court;
                    SqlQuery.Parameters.Add("@Sequence", SqlDbType.Int).Value = Sequence;
                    SqlQuery.Parameters.Add("@numberOfSets", SqlDbType.Int).Value = numberOfSets;
                    SqlQuery.Parameters.Add("@winningSide", SqlDbType.VarChar).Value = winningSide;
                    SqlQuery.Parameters.Add("@statusText", SqlDbType.VarChar).Value = statusText;
                    SqlQuery.Parameters.Add("@Comment", SqlDbType.VarChar).Value = Comment;
                    SqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = timeStamp;
                    SqlQuery.Parameters.Add("@sideOnePlayer1Id", SqlDbType.Int).Value = sideOnePlayer1Id;
                    SqlQuery.Parameters.Add("@sideOnePlayer1Lastname", SqlDbType.VarChar).Value = sideOnePlayer1Lastname;
                    SqlQuery.Parameters.Add("@sideOnePlayer1Firstname", SqlDbType.VarChar).Value = sideOnePlayer1Firstname;
                    SqlQuery.Parameters.Add("@sideOnePlayer1Country", SqlDbType.VarChar).Value = sideOnePlayer1Country;
                    SqlQuery.Parameters.Add("@sideOnePlayer1Seed", SqlDbType.Int).Value = sideOnePlayer1Seed;
                    SqlQuery.Parameters.Add("@sideOnePlayer2Id", SqlDbType.Int).Value = sideOnePlayer2Id;
                    SqlQuery.Parameters.Add("@sideOnePlayer2Lastname", SqlDbType.VarChar).Value = sideOnePlayer2Lastname;
                    SqlQuery.Parameters.Add("@sideOnePlayer2Firstname", SqlDbType.VarChar).Value = sideOnePlayer2Firstname;
                    SqlQuery.Parameters.Add("@sideOnePlayer2Country", SqlDbType.VarChar).Value = sideOnePlayer2Country;
                    SqlQuery.Parameters.Add("@sideOnePlayer2Seed", SqlDbType.Int).Value = sideOnePlayer2Seed;
                    SqlQuery.Parameters.Add("@sideTwoPlayer1Id", SqlDbType.Int).Value = sideTwoPlayer1Id;
                    SqlQuery.Parameters.Add("@sideTwoPlayer1Lastname", SqlDbType.VarChar).Value = sideTwoPlayer1Lastname;
                    SqlQuery.Parameters.Add("@sideTwoPlayer1Firstname", SqlDbType.VarChar).Value = sideTwoPlayer1Firstname;
                    SqlQuery.Parameters.Add("@sideTwoPlayer1Country", SqlDbType.VarChar).Value = sideTwoPlayer1Country;
                    SqlQuery.Parameters.Add("@sideTwoPlayer1Seed", SqlDbType.Int).Value = sideTwoPlayer1Seed;
                    SqlQuery.Parameters.Add("@sideTwoPlayer2Id", SqlDbType.Int).Value = sideTwoPlayer2Id;
                    SqlQuery.Parameters.Add("@sideTwoPlayer2Lastname", SqlDbType.VarChar).Value = sideTwoPlayer2Lastname;
                    SqlQuery.Parameters.Add("@sideTwoPlayer2Firstname", SqlDbType.VarChar).Value = sideTwoPlayer2Firstname;
                    SqlQuery.Parameters.Add("@sideTwoPlayer2Country", SqlDbType.VarChar).Value = sideTwoPlayer2Country;
                    SqlQuery.Parameters.Add("@sideTwoPlayer2Seed", SqlDbType.Int).Value = sideTwoPlayer2Seed;
                    SqlQuery.ExecuteNonQuery();
                }

                _logger.Info("Processing of matches for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processMatches()", ex);

                Processed = false;
                curRun.Errors.Add(fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool processOrderOfPlay(string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                _logger.Info("Processing of Order Of Play for file " + fileName + " started.");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("OrderOfPlay"))
                {
                    int tournamentId = -1;
                    string Court = "";
                    DateTime Date = DateTime.Now;

                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "id":
                                tournamentId = Convert.ToInt32(MainAttribute.Value.ToString().Replace("C", ""));
                                break;
                            case "date":
                                Date = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode CourtNode in MainNode.SelectNodes("Court"))
                    {
                        foreach (XmlAttribute CourtAttribute in CourtNode.Attributes)
                        {
                            switch (CourtAttribute.Name)
                            {
                                case "court":
                                    Court = CourtAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode SessionNode in CourtNode.SelectNodes("Session"))
                        {
                            foreach (XmlNode MatchNode in SessionNode.SelectNodes("Match"))
                            {
                                int matchId = -1;
                                int Sequence = -1;
                                string compType = "";
                                string roundType = "";
                                int numberOfSets = -1;

                                int sideOnePlayer1Id = -1;
                                int sideOnePlayer2Id = -1;
                                int sideTwoPlayer1Id = -1;
                                int sideTwoPlayer2Id = -1;

                                string sideOnePlayer1Lastname = "";
                                string sideOnePlayer2Lastname = "";
                                string sideTwoPlayer1Lastname = "";
                                string sideTwoPlayer2Lastname = "";

                                string sideOnePlayer1Firstname = "";
                                string sideOnePlayer2Firstname = "";
                                string sideTwoPlayer1Firstname = "";
                                string sideTwoPlayer2Firstname = "";

                                string sideOnePlayer1Country = "";
                                string sideOnePlayer2Country = "";
                                string sideTwoPlayer1Country = "";
                                string sideTwoPlayer2Country = "";

                                int sideOnePlayer1Seed = 1000;
                                int sideOnePlayer2Seed = 1000;
                                int sideTwoPlayer1Seed = 1000;
                                int sideTwoPlayer2Seed = 1000;

                                foreach (XmlAttribute MatchAttribute in MatchNode.Attributes)
                                {
                                    switch (MatchAttribute.Name)
                                    {
                                        case "matchid":
                                            matchId = Convert.ToInt32(MatchAttribute.Value.ToString());
                                            break;
                                        case "sequence":
                                            int tempSequence;
                                            var parsed = Int32.TryParse(MatchAttribute.Value, out tempSequence);
                                            if (parsed)
                                            {
                                                Sequence = tempSequence;
                                            }
                                            else
                                            {
                                                Sequence = -1;
                                            }
                                            break;
                                        case "comptype":
                                            compType = MatchAttribute.Value.ToString();
                                            break;
                                        case "roundtype":
                                            roundType = MatchAttribute.Value.ToString();
                                            break;
                                        case "numberofsets":
                                            numberOfSets = Convert.ToInt32(MatchAttribute.Value.ToString());
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                foreach (XmlNode playerNode in MatchNode.SelectNodes("SideOne"))
                                {
                                    foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerOne"))
                                    {
                                        foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                        {
                                            switch (playerAttribute.Name)
                                            {
                                                case "playerid":
                                                    sideOnePlayer1Id = Convert.ToInt32(playerAttribute.Value);
                                                    break;
                                                case "lastname":
                                                    sideOnePlayer1Lastname = playerAttribute.Value.ToString();
                                                    break;
                                                case "firstname":
                                                    sideOnePlayer1Firstname = playerAttribute.Value.ToString();
                                                    break;
                                                case "country":
                                                    sideOnePlayer1Country = playerAttribute.Value.ToString();
                                                    break;
                                                case "seed":
                                                    try
                                                    {
                                                        sideOnePlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                                    }
                                                    catch (Exception seedException)
                                                    {
                                                        _logger.Error("Error occurred in processOrderOfPlay()", seedException);

                                                        sideOnePlayer1Seed = 1000;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }

                                    foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerTwo"))
                                    {
                                        foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                        {
                                            switch (playerAttribute.Name)
                                            {
                                                case "playerid":
                                                    sideOnePlayer2Id = Convert.ToInt32(playerAttribute.Value);
                                                    break;
                                                case "lastname":
                                                    sideOnePlayer2Lastname = playerAttribute.Value.ToString();
                                                    break;
                                                case "firstname":
                                                    sideOnePlayer2Firstname = playerAttribute.Value.ToString();
                                                    break;
                                                case "country":
                                                    sideOnePlayer2Country = playerAttribute.Value.ToString();
                                                    break;
                                                case "seed":
                                                    try
                                                    {
                                                        sideOnePlayer2Seed = Convert.ToInt32(playerAttribute.Value);
                                                    }
                                                    catch (Exception seedException)
                                                    {
                                                        _logger.Error("Error occurred in processOrderOfPlay()", seedException);

                                                        sideOnePlayer2Seed = 1000;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }

                                foreach (XmlNode playerNode in MatchNode.SelectNodes("SideTwo"))
                                {
                                    foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerOne"))
                                    {
                                        foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                        {
                                            switch (playerAttribute.Name)
                                            {
                                                case "playerid":
                                                    sideTwoPlayer1Id = Convert.ToInt32(playerAttribute.Value);
                                                    break;
                                                case "lastname":
                                                    sideTwoPlayer1Lastname = playerAttribute.Value.ToString();
                                                    break;
                                                case "firstname":
                                                    sideTwoPlayer1Firstname = playerAttribute.Value.ToString();
                                                    break;
                                                case "country":
                                                    sideTwoPlayer1Country = playerAttribute.Value.ToString();
                                                    break;
                                                case "seed":
                                                    try
                                                    {
                                                        sideTwoPlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                                    }
                                                    catch (Exception seedException)
                                                    {
                                                        _logger.Error("Error occurred in processOrderOfPlay()", seedException);

                                                        sideTwoPlayer1Seed = 1000;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }

                                    foreach (XmlNode playerSubNode in playerNode.SelectNodes("PlayerTwo"))
                                    {
                                        foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                        {
                                            switch (playerAttribute.Name)
                                            {
                                                case "playerid":
                                                    sideTwoPlayer2Id = Convert.ToInt32(playerAttribute.Value);
                                                    break;
                                                case "lastname":
                                                    sideTwoPlayer2Lastname = playerAttribute.Value.ToString();
                                                    break;
                                                case "firstname":
                                                    sideTwoPlayer2Firstname = playerAttribute.Value.ToString();
                                                    break;
                                                case "country":
                                                    sideTwoPlayer2Country = playerAttribute.Value.ToString();
                                                    break;
                                                case "seed":
                                                    try
                                                    {
                                                        sideTwoPlayer2Seed = Convert.ToInt32(playerAttribute.Value);
                                                    }
                                                    catch (Exception seedException)
                                                    {
                                                        _logger.Error("Error occurred in processOrderOfPlay()", seedException);

                                                        sideTwoPlayer2Seed = 1000;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }

                                SqlQuery = new SqlCommand("Select Count(Id) From tennis.dbo.pa_Matches Where (Id = @matchId)", dbConn);
                                SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId;
                                int Present = Convert.ToInt32(SqlQuery.ExecuteScalar());

                                if (Present > 0)
                                {
                                    SqlQuery = new SqlCommand(@"Update tennis.dbo.pa_Matches Set tournamentId = @tournamentId, Type = @compType, roundType = @roundType, Date = @Date,
                                                                Court = @Court, Sequence = @Sequence, Sets = @numberOfSets, timeStamp = @timeStamp, sideOnePlayer1Id = @sideOnePlayer1Id,
                                                                sideOnePlayer1Lastname = @sideOnePlayer1Lastname, sideOnePlayer1Firstname = @sideOnePlayer1Firstname,
                                                                sideOnePlayer1Country = @sideOnePlayer1Country, sideOnePlayer2Id = @sideOnePlayer2Id, sideOnePlayer2Lastname = @sideOnePlayer2Lastname,
                                                                sideOnePlayer2Firstname = @sideOnePlayer2Firstname, sideOnePlayer2Country = @sideOnePlayer2Country, sideTwoPlayer1Id = @sideTwoPlayer1Id,
                                                                sideTwoPlayer1Lastname = @sideTwoPlayer1Lastname, sideTwoPlayer1Firstname = @sideTwoPlayer1Firstname,
                                                                sideTwoPlayer1Country = @sideTwoPlayer1Country, sideTwoPlayer2Id = @sideTwoPlayer2Id, sideTwoPlayer2Lastname = @sideTwoPlayer2Lastname,
                                                                sideTwoPlayer2Firstname = @sideTwoPlayer2Firstname, sideTwoPlayer2Country = @sideTwoPlayer2Country, sideOnePlayer1Seed = @sideOnePlayer1Seed,
                                                                sideOnePlayer2Seed = @sideOnePlayer2Seed, sideTwoPlayer1Seed = @sideTwoPlayer1Seed, sideTwoPlayer2Seed = @sideTwoPlayer2Seed 
                                                                Where (Id = @matchId)", dbConn);
                                }
                                else
                                {
                                    SqlQuery = new SqlCommand(@"Insert Into tennis.dbo.pa_Matches (Id, tournamentId, Type, roundType, Date, Court, Sequence, Sets, timeStamp, sideOnePlayer1Id,
                                                                sideOnePlayer1Lastname, sideOnePlayer1Firstname, sideOnePlayer1Country, sideOnePlayer2Id, sideOnePlayer2Lastname, sideOnePlayer2Firstname,
                                                                sideOnePlayer2Country, sideTwoPlayer1Id, sideTwoPlayer1Lastname, sideTwoPlayer1Firstname, sideTwoPlayer1Country, sideTwoPlayer2Id,
                                                                sideTwoPlayer2Lastname, sideTwoPlayer2Firstname, sideTwoPlayer2Country, sideOnePlayer1Seed, sideOnePlayer2Seed, sideTwoPlayer1Seed,
                                                                sideTwoPlayer2Seed) Values (@matchId, @tournamentId, @compType, @roundType, @Date, @Court, @Sequence, @numberOfSets, @timeStamp,
                                                                @sideOnePlayer1Id, @sideOnePlayer1Lastname, @sideOnePlayer1Firstname, @sideOnePlayer1Country, @sideOnePlayer2Id,
                                                                @sideOnePlayer2Lastname, @sideOnePlayer2Firstname, @sideOnePlayer2Country, @sideTwoPlayer1Id, @sideTwoPlayer1Lastname,
                                                                @sideTwoPlayer1Firstname, @sideTwoPlayer1Country, @sideTwoPlayer2Id, @sideTwoPlayer2Lastname, @sideTwoPlayer2Firstname,
                                                                @sideTwoPlayer2Country, @sideOnePlayer1Seed, @sideOnePlayer2Seed, @sideTwoPlayer1Seed, @sideTwoPlayer2Seed)", dbConn);
                                }
                                SqlQuery.Parameters.Add("@matchId", SqlDbType.Int).Value = matchId;
                                SqlQuery.Parameters.Add("@tournamentId", SqlDbType.Int).Value = tournamentId;
                                SqlQuery.Parameters.Add("@compType", SqlDbType.VarChar).Value = compType;
                                SqlQuery.Parameters.Add("@roundType", SqlDbType.VarChar).Value = roundType;
                                SqlQuery.Parameters.Add("@Court", SqlDbType.VarChar).Value = Court;
                                SqlQuery.Parameters.Add("@Sequence", SqlDbType.Int).Value = Sequence;
                                SqlQuery.Parameters.Add("@numberOfSets", SqlDbType.Int).Value = numberOfSets;
                                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = DateTime.Now;
                                SqlQuery.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                                SqlQuery.Parameters.Add("@sideOnePlayer1Id", SqlDbType.Int).Value = sideOnePlayer1Id;
                                SqlQuery.Parameters.Add("@sideOnePlayer1Lastname", SqlDbType.VarChar).Value = sideOnePlayer1Lastname;
                                SqlQuery.Parameters.Add("@sideOnePlayer1Firstname", SqlDbType.VarChar).Value = sideOnePlayer1Firstname;
                                SqlQuery.Parameters.Add("@sideOnePlayer1Country", SqlDbType.VarChar).Value = sideOnePlayer1Country;
                                SqlQuery.Parameters.Add("@sideOnePlayer1Seed", SqlDbType.Int).Value = sideOnePlayer1Seed;
                                SqlQuery.Parameters.Add("@sideOnePlayer2Id", SqlDbType.Int).Value = sideOnePlayer2Id;
                                SqlQuery.Parameters.Add("@sideOnePlayer2Lastname", SqlDbType.VarChar).Value = sideOnePlayer2Lastname;
                                SqlQuery.Parameters.Add("@sideOnePlayer2Firstname", SqlDbType.VarChar).Value = sideOnePlayer2Firstname;
                                SqlQuery.Parameters.Add("@sideOnePlayer2Country", SqlDbType.VarChar).Value = sideOnePlayer2Country;
                                SqlQuery.Parameters.Add("@sideOnePlayer2Seed", SqlDbType.Int).Value = sideOnePlayer2Seed;
                                SqlQuery.Parameters.Add("@sideTwoPlayer1Id", SqlDbType.Int).Value = sideTwoPlayer1Id;
                                SqlQuery.Parameters.Add("@sideTwoPlayer1Lastname", SqlDbType.VarChar).Value = sideTwoPlayer1Lastname;
                                SqlQuery.Parameters.Add("@sideTwoPlayer1Firstname", SqlDbType.VarChar).Value = sideTwoPlayer1Firstname;
                                SqlQuery.Parameters.Add("@sideTwoPlayer1Country", SqlDbType.VarChar).Value = sideTwoPlayer1Country;
                                SqlQuery.Parameters.Add("@sideTwoPlayer1Seed", SqlDbType.Int).Value = sideTwoPlayer1Seed;
                                SqlQuery.Parameters.Add("@sideTwoPlayer2Id", SqlDbType.Int).Value = sideTwoPlayer2Id;
                                SqlQuery.Parameters.Add("@sideTwoPlayer2Lastname", SqlDbType.VarChar).Value = sideTwoPlayer2Lastname;
                                SqlQuery.Parameters.Add("@sideTwoPlayer2Firstname", SqlDbType.VarChar).Value = sideTwoPlayer2Firstname;
                                SqlQuery.Parameters.Add("@sideTwoPlayer2Country", SqlDbType.VarChar).Value = sideTwoPlayer2Country;
                                SqlQuery.Parameters.Add("@sideTwoPlayer2Seed", SqlDbType.Int).Value = sideTwoPlayer2Seed;
                                SqlQuery.ExecuteNonQuery();
                            }
                        }
                    }
                }

                _logger.Info("Processing of Order Of Play for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processOrderOfPlay()", ex);

                Processed = false;
                curRun.Errors.Add(fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool processDraw(string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                _logger.Info("Processing of Draw for file " + fileName + " started.");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("Draws"))
                {
                    int tournamentId = -1;
                    string tournamentType = "";
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;

                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "id":
                                tournamentId = Convert.ToInt32(MainAttribute.Value.ToString().Replace("C", ""));
                                break;
                            case "startdate":
                                startDate = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            case "enddate":
                                endDate = new DateTime(Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[2]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[1]),
                                    Convert.ToInt32(MainAttribute.Value.Replace("/", "-").Split('-')[0]));
                                break;
                            case "tournamenttype":
                                tournamentType = MainAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_Draws Where (tournamentId = @tournamentId) And (Year(startDate) = @Year)", dbConn);
                    SqlQuery.Parameters.Add("@tournamentId", SqlDbType.Int).Value = tournamentId;
                    SqlQuery.Parameters.Add("@Year", SqlDbType.Int).Value = DateTime.Now.Year;
                    SqlQuery.ExecuteNonQuery();

                    foreach (XmlNode DrawNode in MainNode.SelectNodes("Draw"))
                    {
                        string drawId = "";
                        string Status = "";
                        DateTime Date = DateTime.Now;
                        string compType = "";
                        string roundType = "";
                        int roundOrder = -1;
                        int drawOrder = -1;
                        int Sets = -1;

                        foreach (XmlAttribute DrawAttribute in DrawNode.Attributes)
                        {
                            switch (DrawAttribute.Name)
                            {
                                case "drawid":
                                    drawId = DrawAttribute.Value.ToString();
                                    break;
                                case "status":
                                    Status = DrawAttribute.Value.ToString();
                                    break;
                                case "date":
                                    if (DrawAttribute.Value.ToString().IndexOf("//") >= 0)
                                    {
                                        Date = new DateTime(1900, 1, 1);
                                    }
                                    else
                                    {
                                        Date = new DateTime(Convert.ToInt32(DrawAttribute.Value.Replace("/", "-").Split('-')[2]),
                                            Convert.ToInt32(DrawAttribute.Value.Replace("/", "-").Split('-')[1]),
                                            Convert.ToInt32(DrawAttribute.Value.Replace("/", "-").Split('-')[0]));
                                    }
                                    break;
                                case "comptype":
                                    compType = DrawAttribute.Value.ToString();
                                    break;
                                case "roundtype":
                                    roundType = DrawAttribute.Value.ToString();
                                    break;
                                case "roundorder":
                                    roundOrder = Convert.ToInt32(DrawAttribute.Value.ToString());
                                    break;
                                case "draworder":
                                    drawOrder = Convert.ToInt32(DrawAttribute.Value.ToString());
                                    break;
                                case "numberofsets":
                                    Sets = Convert.ToInt32(DrawAttribute.Value.ToString());
                                    break;
                                default:
                                    break;
                            }
                        }

                        int sideOnePlayer1Id = -1;
                        int sideOnePlayer2Id = -1;
                        int sideTwoPlayer1Id = -1;
                        int sideTwoPlayer2Id = -1;

                        string sideOnePlayer1Lastname = "";
                        string sideOnePlayer2Lastname = "";
                        string sideTwoPlayer1Lastname = "";
                        string sideTwoPlayer2Lastname = "";

                        string sideOnePlayer1Firstname = "";
                        string sideOnePlayer2Firstname = "";
                        string sideTwoPlayer1Firstname = "";
                        string sideTwoPlayer2Firstname = "";

                        string sideOnePlayer1Country = "";
                        string sideOnePlayer2Country = "";
                        string sideTwoPlayer1Country = "";
                        string sideTwoPlayer2Country = "";

                        int sideOnePlayer1Seed = -1;
                        int sideOnePlayer2Seed = -1;
                        int sideTwoPlayer1Seed = -1;
                        int sideTwoPlayer2Seed = -1;

                        foreach (XmlNode playerNode in DrawNode.SelectNodes("SideOne"))
                        {
                            foreach (XmlNode playerSubNode in playerNode.SelectNodes("Player"))
                            {
                                foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                {
                                    switch (playerAttribute.Name)
                                    {
                                        case "playerid":
                                            int tempPlayerId;
                                            var parsed = Int32.TryParse(playerAttribute.Value, out tempPlayerId);
                                            if (parsed)
                                            {
                                                sideOnePlayer1Id = tempPlayerId;
                                            }
                                            else
                                            {
                                                sideOnePlayer1Lastname = playerAttribute.Value;
                                            }
                                            break;
                                        case "lastname":
                                            if (sideOnePlayer1Lastname != "BYE" && playerAttribute.Value != "QUALIFIER")
                                            {
                                                sideOnePlayer1Lastname = playerAttribute.Value.ToString();
                                            }
                                            break;
                                        case "firstname":
                                            sideOnePlayer1Firstname = playerAttribute.Value.ToString();
                                            break;
                                        case "country":
                                            sideOnePlayer1Country = playerAttribute.Value.ToString();
                                            break;
                                        case "seed":
                                            sideOnePlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }

                        foreach (XmlNode playerNode in DrawNode.SelectNodes("SideTwo"))
                        {
                            foreach (XmlNode playerSubNode in playerNode.SelectNodes("Player"))
                            {
                                foreach (XmlAttribute playerAttribute in playerSubNode.Attributes)
                                {
                                    switch (playerAttribute.Name)
                                    {
                                        case "playerid":
                                            int tempPlayerId;
                                            var parsed = Int32.TryParse(playerAttribute.Value, out tempPlayerId);
                                            if (parsed)
                                            {
                                                sideTwoPlayer1Id = tempPlayerId;
                                            }
                                            else
                                            {
                                                sideTwoPlayer1Lastname = playerAttribute.Value;
                                            }
                                            break;
                                        case "lastname":
                                            if (sideTwoPlayer1Lastname != "BYE" && playerAttribute.Value != "QUALIFIER")
                                            {
                                                sideTwoPlayer1Lastname = playerAttribute.Value.ToString();
                                            }
                                            break;
                                        case "firstname":
                                            sideTwoPlayer1Firstname = playerAttribute.Value.ToString();
                                            break;
                                        case "country":
                                            sideTwoPlayer1Country = playerAttribute.Value.ToString();
                                            break;
                                        case "seed":
                                            sideTwoPlayer1Seed = Convert.ToInt32(playerAttribute.Value);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }

                        SqlQuery = new SqlCommand(@"Insert Into tennis.dbo.pa_Draws (tournamentId, tournamentType, startDate, endDate, Date, drawId, status, compType, roundType, roundOrder, drawOrder,
                                                    Sets, sideOnePlayer1Id, sideOnePlayer1Lastname, sideOnePlayer1Firstname, sideOnePlayer1Country, sideOnePlayer1Seed, sideOnePlayer2Id,
                                                    sideOnePlayer2Lastname, sideOnePlayer2Firstname, sideOnePlayer2Country, sideOnePlayer2Seed, sideTwoPlayer1Id, sideTwoPlayer1Lastname,
                                                    sideTwoPlayer1Firstname, sideTwoPlayer1Country, sideTwoPlayer1Seed, sideTwoPlayer2Id, sideTwoPlayer2Lastname, sideTwoPlayer2Firstname,
                                                    sideTwoPlayer2Country, sideTwoPlayer2Seed) Values (@tournamentId, @tournamentType, @startDate, @endDate, @Date, @drawId, @status, @compType,
                                                    @roundType, @roundOrder, @drawOrder, @Sets, @sideOnePlayer1Id, @sideOnePlayer1Lastname, @sideOnePlayer1Firstname, @sideOnePlayer1Country,
                                                    @sideOnePlayer1Seed, @sideOnePlayer2Id, @sideOnePlayer2Lastname, @sideOnePlayer2Firstname, @sideOnePlayer2Country,
                                                    @sideOnePlayer2Seed, @sideTwoPlayer1Id, @sideTwoPlayer1Lastname, @sideTwoPlayer1Firstname, @sideTwoPlayer1Country, @sideTwoPlayer1Seed,
                                                    @sideTwoPlayer2Id, @sideTwoPlayer2Lastname, @sideTwoPlayer2Firstname, @sideTwoPlayer2Country, @sideTwoPlayer2Seed)", dbConn);
                        SqlQuery.Parameters.Add("@tournamentId", SqlDbType.Int).Value = tournamentId;
                        SqlQuery.Parameters.Add("@tournamentType", SqlDbType.VarChar).Value = tournamentType;
                        SqlQuery.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        SqlQuery.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        SqlQuery.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                        SqlQuery.Parameters.Add("@drawId", SqlDbType.VarChar).Value = drawId;
                        SqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = Status;
                        SqlQuery.Parameters.Add("@compType", SqlDbType.VarChar).Value = compType;
                        SqlQuery.Parameters.Add("@roundType", SqlDbType.VarChar).Value = roundType;
                        SqlQuery.Parameters.Add("@roundOrder", SqlDbType.Int).Value = roundOrder;
                        SqlQuery.Parameters.Add("@drawOrder", SqlDbType.Int).Value = drawOrder;
                        SqlQuery.Parameters.Add("@Sets", SqlDbType.Int).Value = Sets;
                        SqlQuery.Parameters.Add("@sideOnePlayer1Id", SqlDbType.Int).Value = sideOnePlayer1Id;
                        SqlQuery.Parameters.Add("@sideOnePlayer1Lastname", SqlDbType.VarChar).Value = sideOnePlayer1Lastname;
                        SqlQuery.Parameters.Add("@sideOnePlayer1Firstname", SqlDbType.VarChar).Value = sideOnePlayer1Firstname;
                        SqlQuery.Parameters.Add("@sideOnePlayer1Country", SqlDbType.VarChar).Value = sideOnePlayer1Country;
                        SqlQuery.Parameters.Add("@sideOnePlayer1Seed", SqlDbType.Int).Value = sideOnePlayer1Seed;
                        SqlQuery.Parameters.Add("@sideOnePlayer2Id", SqlDbType.Int).Value = sideOnePlayer2Id;
                        SqlQuery.Parameters.Add("@sideOnePlayer2Lastname", SqlDbType.VarChar).Value = sideOnePlayer2Lastname;
                        SqlQuery.Parameters.Add("@sideOnePlayer2Firstname", SqlDbType.VarChar).Value = sideOnePlayer2Firstname;
                        SqlQuery.Parameters.Add("@sideOnePlayer2Country", SqlDbType.VarChar).Value = sideOnePlayer2Country;
                        SqlQuery.Parameters.Add("@sideOnePlayer2Seed", SqlDbType.Int).Value = sideOnePlayer2Seed;
                        SqlQuery.Parameters.Add("@sideTwoPlayer1Id", SqlDbType.Int).Value = sideTwoPlayer1Id;
                        SqlQuery.Parameters.Add("@sideTwoPlayer1Lastname", SqlDbType.VarChar).Value = sideTwoPlayer1Lastname;
                        SqlQuery.Parameters.Add("@sideTwoPlayer1Firstname", SqlDbType.VarChar).Value = sideTwoPlayer1Firstname;
                        SqlQuery.Parameters.Add("@sideTwoPlayer1Country", SqlDbType.VarChar).Value = sideTwoPlayer1Country;
                        SqlQuery.Parameters.Add("@sideTwoPlayer1Seed", SqlDbType.Int).Value = sideTwoPlayer1Seed;
                        SqlQuery.Parameters.Add("@sideTwoPlayer2Id", SqlDbType.Int).Value = sideTwoPlayer2Id;
                        SqlQuery.Parameters.Add("@sideTwoPlayer2Lastname", SqlDbType.VarChar).Value = sideTwoPlayer2Lastname;
                        SqlQuery.Parameters.Add("@sideTwoPlayer2Firstname", SqlDbType.VarChar).Value = sideTwoPlayer2Firstname;
                        SqlQuery.Parameters.Add("@sideTwoPlayer2Country", SqlDbType.VarChar).Value = sideTwoPlayer2Country;
                        SqlQuery.Parameters.Add("@sideTwoPlayer2Seed", SqlDbType.Int).Value = sideTwoPlayer2Seed;
                        SqlQuery.ExecuteNonQuery();
                    }
                }

                _logger.Info("Processing of Draw for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processDraw()", ex);

                Processed = false;
                curRun.Errors.Add(fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool processRankings(string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                _logger.Info("Processing of Rankings for file " + fileName + " started.");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("c:/SuperSport/Tennis/Xml/Temp/" + fileName);

                foreach (XmlNode MainNode in xmlDoc.SelectNodes("Rankings"))
                {
                    string Type = "";
                    DateTime Date = DateTime.Now;
                    string queryString = "";

                    foreach (XmlAttribute MainAttribute in MainNode.Attributes)
                    {
                        switch (MainAttribute.Name)
                        {
                            case "type":
                                Type = MainAttribute.Value.ToString();
                                break;
                            case "date":
                                string tmpDate = MainAttribute.Value.ToString();
                                int timeStampYear = Convert.ToInt32(tmpDate.Substring(4, 4));
                                int timeStampMonth = Convert.ToInt32(tmpDate.Substring(2, 2));
                                int timeStampDay = Convert.ToInt32(tmpDate.Substring(0, 2));
                                Date = new DateTime(timeStampYear, timeStampMonth, timeStampDay, 0, 0, 0);
                                break;
                            default:
                                break;
                        }
                    }

                    SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_Rankings Where (Type = @type)", dbConn);
                    SqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = Type;
                    SqlQuery.ExecuteNonQuery();

                    foreach (XmlNode RankingNode in MainNode.SelectNodes("Ranking"))
                    {
                        int Ranking = -1;
                        int raceRanking = -1;
                        int playerId = -1;
                        string Lastname = "";
                        string Firstname = "";
                        string Points = "";
                        string Movement = "";
                        string racePoints = "";
                        string raceMovement = "";
                        string country = "";
                        foreach (XmlAttribute RankingAttribute in RankingNode.Attributes)
                        {
                            switch (RankingAttribute.Name)
                            {
                                case "ranking":
                                    Ranking = Convert.ToInt32(RankingAttribute.Value.ToString());
                                    break;
                                case "raceRank":
                                    raceRanking = Convert.ToInt32(RankingAttribute.Value.ToString());
                                    break;
                                case "playerid":
                                    playerId = Convert.ToInt32(RankingAttribute.Value.ToString());
                                    break;
                                case "lastname":
                                    Lastname = RankingAttribute.Value.ToString();
                                    break;
                                case "firstname":
                                    Firstname = RankingAttribute.Value.ToString();
                                    break;
                                case "points":
                                    Points = RankingAttribute.Value.ToString();
                                    break;
                                case "movement":
                                    Movement = GetMovement(RankingAttribute.Value.ToString());
                                    break;
                                case "racePoints":
                                    racePoints = RankingAttribute.Value.ToString();
                                    break;
                                case "raceMovement":
                                    raceMovement = GetMovement(RankingAttribute.Value.ToString());
                                    break;
                                case "country":
                                    country = RankingAttribute.Value.ToString().ToUpper();
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (queryString != "")
                        {
                            queryString += Environment.NewLine;
                        }
                        queryString += @"Insert Into tennis.dbo.pa_Rankings (type, date, ranking, raceRanking, playerId, lastname, firstname, points, movement, racePoints, raceMovement, country)
                                        Values ('" + Type + "', '" + DateTime.Now.ToString("yyyy-MM-dd") + "', " + Ranking + ", " + raceRanking + ", " + playerId + ", '" + 
                                        Lastname.Replace("'", "''") + "', '" + Firstname.Replace("'", "''") + "', '" + Points + "', '" + Movement + "', '" + racePoints + "', '" + raceMovement + "', '" + country +"');";
                    }

                    if (queryString != "")
                    {
                        SqlQuery = new SqlCommand(queryString, dbConn);
                        SqlQuery.ExecuteNonQuery();
                    }
                }

                _logger.Info("Processing of Rankings for file " + fileName + " ended.");

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in processRankings()", ex);

                Processed = false;
                curRun.Errors.Add(fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private string GetMovement(string movementString)
        {
            return movementString.Replace("+", string.Empty);
        }

        private void archiveFiles()
        {
            if (!(File.Exists("c:/SuperSport/Tennis/Xml/History/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.ToString("yyyyMMdd") + ".zip")))
            {
                try
                {
                    _logger.Info("Archiving data started for " + DateTime.Now.ToString("yyyyMMdd"));
                    DiskFolder tmpFolder = new DiskFolder("c:/SuperSport/Tennis/Xml/Processed");
                    ZipArchive tmpArchive = new ZipArchive(new DiskFile("c:/SuperSport/Tennis/Xml/History/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.ToString("yyyyMMdd") + ".zip"));
                    tmpFolder.MoveFilesTo(tmpArchive, true, true);
                    _logger.Info("Archiving data completed for " + DateTime.Now.ToString("yyyyMMdd"));
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in archiveFiles()", ex);

                    curRun.Errors.Add("Zip - " + ex.Message);
                }
            }
        }

        private void checkFolders()
        {
            if (!Directory.Exists("c:/SuperSport"))
            {
                Directory.CreateDirectory("c:/SuperSport");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml/Temp"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml/Temp");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml/Processed"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml/Processed");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml/Unprocessed"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml/Unprocessed");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml/History"))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml/History");
            }
            if (!Directory.Exists("c:/SuperSport/Tennis/Xml/History/" + DateTime.Now.Year.ToString()))
            {
                Directory.CreateDirectory("c:/SuperSport/Tennis/Xml/History/" + DateTime.Now.Year.ToString());
            }
        }
    }

    public class runInfo
    {
        public DateTime Start;
        public DateTime End;
        public int Downloads;
        public int DownloadErrors;
        public List<string> Errors = new List<string>();
    }
}