﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ssTennis.Tests
{
    internal static class TestHelper
    {
        internal const string UNIT_TEST_CATEGORY = "Unit";
        internal const string INTEGRATION_TEST_CATEGORY = "Integration";
    }
}
